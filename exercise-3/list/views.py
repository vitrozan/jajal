from django.shortcuts import redirect, render
from list.models import Item

# Create your views here.
def home_page(request):
    if request.method == 'POST':
        Item.objects.create(text=request.POST['item_text'])
        return redirect('/')

    items = Item.objects.all()
    if (items.count() == 0):
        theComment = "Horee Waktunyaa Mapann Bor"
    elif (items.count() < 5):
        theComment = "sibuk tapi santuyy"
    else:
        theComment = "oh tidak"

    return render(request, 'home.html', {'items': items, 'theComment': theComment})
